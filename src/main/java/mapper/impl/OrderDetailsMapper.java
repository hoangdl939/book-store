/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mapper.impl;

import entity.OrderDetails;
import java.sql.ResultSet;
import mapper.IGenericMapper;

/**
 *
 * @author ADMIN
 */
public class OrderDetailsMapper implements IGenericMapper<OrderDetails>{

    @Override
    public OrderDetails mapRow(ResultSet resultSet) {
        try {
            int id = resultSet.getInt("id");
            int quantity = resultSet.getInt("quantity");
            int bookId = resultSet.getInt("bookId");
            int orderId = resultSet.getInt("orderId");
            
            OrderDetails orderDetails = new OrderDetails(id, quantity, bookId, orderId);
            return orderDetails;
        } catch (Exception e) {
            System.out.println("Wrong at OrderDetailsMapper: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
}
