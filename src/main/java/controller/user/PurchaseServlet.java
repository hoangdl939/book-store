/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import dal.impl.BookDAO;
import dal.impl.OrderDetailsDAO;
import entity.Account;
import entity.Book;
import entity.OrderDetails;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Acer
 */
public class PurchaseServlet extends HttpServlet {
@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderDetailsDAO orderDetailsDAO = new OrderDetailsDAO();
        BookDAO bookDAO = new BookDAO();
        HttpSession session = request.getSession();

        //get account
        Account account = (Account) session.getAttribute("account");

        //get list book purchase by account id
        HashMap<Integer, Book> hashmapBook = (HashMap<Integer, Book>) session.getAttribute("hashmapBook");

        if (hashmapBook == null) {
            hashmapBook = bookDAO.findBooksByAccountId(account.getId());
        }
        //get list orderDetails
        List<OrderDetails> listOrderDetails
                = listOrderDetails = orderDetailsDAO.findsByAccountId(account.getId());

        //set attribute to session
        session.setAttribute("listOrderDetails", listOrderDetails);
        session.setAttribute("hashmapBook", hashmapBook);

        response.sendRedirect("dashboard?page=purchase");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
