/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import dal.impl.BookDAO;
import entity.Book;
import entity.ItemCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Acer
 */
public class CartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CartServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CartServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("view/user/cart/cartDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null || action.isEmpty()) {
            response.sendRedirect("home");
        }

        switch (action) {
            case "add-product":
                addProduct(request, response);
                response.sendRedirect("cart");
                break;
            case "change-quantity":
                changeQuantity(request, response);
                response.sendRedirect("cart");
                break;
            case "delete":
                deleteItem(request, response);
                response.sendRedirect("cart");
                break;
            default:
                response.sendRedirect("home");
        }
    }

    private void addProduct(HttpServletRequest request, HttpServletResponse response) {
        BookDAO dao = new BookDAO();
        HttpSession session = request.getSession();

        //get quantity
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        //get id
        int id = Integer.parseInt(request.getParameter("id"));

        //find product by id
        Book book = dao.findOneById(id);
        //tao ra 1 doi tuong ItemCart
        ItemCart itemCart = new ItemCart(book, 3);

        HashMap<Integer, ItemCart> cart = (HashMap<Integer, ItemCart>) session.getAttribute("cart");
        //kiem tra neu chua tung co cart
        if (cart == null) {
            cart = new HashMap<>();
        }
        //them ItemCart vao ben trong cart
        addOrderDetails(itemCart, cart, 3);
        //luu tru cart len session
        session.setAttribute("cart", cart);
    }

    private void addOrderDetails(ItemCart itemCart, HashMap<Integer, ItemCart> cart,
            int quantity) {
        int idProductInOrderDetails = itemCart.getBook().getId();
        //kiem tra xem trong cart da tung co sn pham hay chua
        //neu da co san pham roi, thi + don` so luong
        if (cart.containsKey(idProductInOrderDetails)) {
            int oldQUantity = cart.get(idProductInOrderDetails).getQuantity();
            ItemCart orderDetailsInCart = cart.get(idProductInOrderDetails);
            orderDetailsInCart.setQuantity(oldQUantity + quantity);
            //neu chua thi moi mang di put
        } else {
            cart.put(idProductInOrderDetails, itemCart);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void changeQuantity(HttpServletRequest request, HttpServletResponse response) {
        BookDAO dao = new BookDAO();
        HttpSession session = request.getSession();
        //get id
        int id = Integer.parseInt(request.getParameter("id"));
        //get quantity
        int quantity = Integer.parseInt(request.getParameter("quantity"));

        //find product by id
        Book book = dao.findOneById(id);

        HashMap<Integer, ItemCart> cart = (HashMap<Integer, ItemCart>) session.getAttribute("cart");
        //kiem tra neu chua tung co cart
        if (cart == null) {
            cart = new HashMap<>();
        }
        //change quantity of product
        ItemCart itemCart = cart.get(id);
        itemCart.setQuantity(quantity);
        //luu tru cart len session
        session.setAttribute("cart", cart);
    }

    private void deleteItem(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        //get id
        int id = Integer.parseInt(request.getParameter("id"));

        HashMap<Integer, ItemCart> cart = (HashMap<Integer, ItemCart>) session.getAttribute("cart");
        cart.remove(id);
        //luu tru cart len session
        session.setAttribute("cart", cart);
    }

}
