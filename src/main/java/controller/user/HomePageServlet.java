/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import biz.impl.BookLogic;
import biz.impl.CategoryLogic;
import constant.CommonConst;
import entity.Book;
import entity.Category;
import entity.PageControl;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Acer
 */
public class HomePageServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BookLogic bookLogic = new BookLogic();
        CategoryLogic categoryLogic = new CategoryLogic();
        //get session
        HttpSession session =request.getSession();
        //get total record
        int totalRecord = bookLogic.findTotalRecord("");

        //get total page
        int totalPage = bookLogic.findTotalPage(totalRecord);
        //get current page
        int page = bookLogic.findCurrrentPage(request);
        //get list book by current page
        List<Book> listByCurrentPage = bookLogic.findBooksByPage(page, "");
        //get list category
        List<Category> listCategories = categoryLogic.findAll();
    
        
        //create instance
        PageControl pageControl = new PageControl(CommonConst.BOOK_RECORD_PER_PAGE,
                totalPage,
                totalRecord,
                page,
                "home?");

        //set attribute
        session.setAttribute("listBook", listByCurrentPage);
        session.setAttribute("listCategories", listCategories);
        request.setAttribute("pageControl", pageControl);
        //redirect to page
        request.getRequestDispatcher("view/user/homepage/index.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
