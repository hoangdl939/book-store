/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.authen;

import biz.impl.AccountLogic;
import constant.CommonConst;
import entity.Account;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Acer
 */
public class LoginServlet extends HttpServlet {



    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("view/common/authen/login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        AccountLogic accountLogic = new AccountLogic();
        HttpSession session = request.getSession();
       //get username
        String username = request.getParameter("username");
        
       //get password
       String password = request.getParameter("password");
       
       //create instance
       Account account  = Account.builder().username(username)
                                           .password(password)
                                           .build();
               
       //check username and password exist
       account = accountLogic.findAccount(account, CommonConst.FIND_ACCOUNT_BY_USERNAME_PASSWORD);
       
        if (account != null) {
            session.setAttribute(CommonConst.SESSION_ACCOUNT, account);
            //go to page coresponding to role
            switch (account.getRoleId()) {
                case CommonConst.ROLE_ADMIN:
                    response.sendRedirect("admin/dashboard");
                    break;
                case CommonConst.ROLE_USER:
                    response.sendRedirect("home");
                    break;
            }
            //account not exist
        } else{
            request.setAttribute("error", "Username or password is incorrect !");
            request.getRequestDispatcher("view/common/authen/login.jsp").forward(request, response);
        }
       //
    }



}
