/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package biz.impl;

import biz.IGenericLogic;
import constant.CommonConst;
import dal.impl.BookDAO;
import entity.Book;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Acer
 */
public class BookLogic implements IGenericLogic<Book> {

    BookDAO dao;

    public BookLogic() {
        dao = new BookDAO();
    }

    @Override
    public List<Book> findAll() {
        List<Book> list = dao.findAll();
        return list;
    }

    public int findCurrrentPage(HttpServletRequest request) {
        int currentPage;

        if (request.getParameter("page") == null) {
            currentPage = 1;
        } else {
            try {
                currentPage = Integer.parseInt(request.getParameter("page"));
            } catch (NumberFormatException e) {
                currentPage = 1;
            }

        }
        return currentPage;

    }



    public int findTotalRecord(String keyword) {
        int totalRecord = dao.findTotalRecord(keyword);
        return totalRecord;
    }

    public int findTotalPage(int totalRecord) {
        int result = totalRecord % CommonConst.BOOK_RECORD_PER_PAGE;
        if (result != 0) {
           return result++;
        } else{
            return result;
        }
    }

    public List<Book> findBooksByPage(int page, String keyword) {
        List<Book> list =dao.findBooksByPage(page, keyword);
        return list;
    }

    public int findTotalRecordByKeyword(String keyword) {
        int totalRecord = dao.findTotalRecord(keyword);
        return totalRecord;
    }

    public int findTotalRecordByCateId(int categoryId) {
        int totalRecord = dao.findTotalRecordByCateId(categoryId);
        return totalRecord;
    }

    public List<Book> findBooksByCategory(int page, int categoryId) {
        List<Book> listByCurrentPage = dao.findBooksByCategory(page, categoryId);
        return listByCurrentPage;
    }

    @Override
    public int insertToDb(Book t) {
        return dao.insertToDb(t);
    }

    @Override
    public void updateToDb(Book book) {
        dao.updateToDb(book);
    }

    @Override
    public void deleteBook(Book t) {
        dao.delete(t);
    }

    public Book findBooksById(int id) {
        return dao.findOneById(id);        
    }

}
